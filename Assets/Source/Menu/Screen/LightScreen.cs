﻿namespace Developer.Screen
{
    using UnityEngine;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

        private void Awake()
        {
            GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
            GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
            lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
            lights.SetState();
        }

        //TODO: According the rules, turn the lights on and off...
        //Hint: The networking functionality is inside the "Test" namespace.
    }
}