﻿namespace Developer.Core
{
    using UnityEngine;

    /// <inheritdoc />
    /// <summary>
    /// Controller which loads all required plugins before loading the first screen for the app.
    /// </summary>
    public sealed class PlatformLauncher : MonoBehaviour
    {
        private const bool UseReporter = false;

        private App _app = null;

        private void Awake()
        {
            if (UseReporter)
            {
                //TODO: Load the reporter...
            }

            _app = new App(GameObject.Find("Canvas"));
            _app.ChangeScreen<Developer.Screen.LoadingScreen>();
        }
    }
}